﻿#include <iostream>
#include <vector>
#include <string>
template<typename T>
class Stack
{
public:
    void push(T);
    T pop();
    void show();
private:
    std::vector<T> V;
};

int main() {
    Stack<int> a;
    a.push(1);  a.push(2);  a.push(3); a.push(4);
    a.show();
    std::cout << "poped out:" << a.pop() << std::endl;
    a.show();

    return 0;
}

template<class T> void Stack<T>::push(T elem)
{
    V.push_back(elem);
}

template<class T> T Stack<T>::pop()
{
    T elem = V.back();
    V.pop_back();
    return elem;
}
template<class T> void Stack<T>::show()
{
    std::cout << "stack:";
    for (auto e : V) std::cout << e << " ";
    std::cout << std::endl;
}